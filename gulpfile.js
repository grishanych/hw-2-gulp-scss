const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const autoprefix = require('gulp-autoprefixer');
const browsersync = require('browser-sync').create();
const cssclean = require('gulp-clean-css');
const concatjs = require('gulp-concat');
const clean = require('gulp-clean');
const jsminify = require('gulp-js-minify');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');
const uglify = require('gulp-terser');
const rename = require('gulp-rename');


function browserServe(stop) {
    browsersync.init({
        server: {
            baseDir: './dist'
        }
    })
    stop()
}

function html() {
    return gulp.src('src/index.html')
        .pipe(gulp.dest('./dist'))
}

function css() {
    return gulp.src('src/styles/*.scss')
        .pipe(sass())
        .pipe(autoprefix({
            cascade: false
        }))
        .pipe(cssclean())
        .pipe(rename('styles.min.css'))
        .pipe(gulp.dest('./dist'))
}

function js() {
    return gulp.src('src/scripts/*.js')
        .pipe(uglify())
        .pipe(jsminify())
        .pipe(concatjs('scripts.min.js'))
        .pipe(gulp.dest('./dist'))
}

function images() {
    return gulp.src('src/images/**/*.*')
        .pipe(imagemin([pngquant()]))
        .pipe(gulp.dest('./dist/images'))
}

function watch() {
    gulp.watch('./src/**/*.*', gulp.parallel(html, css, js, images)).on('all', browsersync.reload)
}

function clearDist() {
    return gulp.src('./dist', { allowEmpty: true }).pipe(clean())
}

exports.build = gulp.parallel(html, css, js, images);
exports.watch = gulp.series(clearDist, this.build, browserServe, watch);